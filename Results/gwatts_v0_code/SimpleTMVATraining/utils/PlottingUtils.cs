﻿using ChallengeData;
using LINQToTreeHelpers.FutureUtils;
using ROOTNET.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LINQToTreeHelpers.PlottingUtils;

namespace SimpleTMVATraining.utils
{
    static class PlottingUtils
    {
        /// <summary>
        /// Make a few standard plots
        /// </summary>
        /// <param name="jets"></param>
        /// <param name="dir"></param>
        public static void MakeStandardPlots (this IQueryable<treeJets> jets, FutureTDirectory dir)
        {
            jets
                .FuturePlot(JetPtPlot, "all")
                .Save(dir);
        }

        #region Plot Specifications
        /// <summary>
        /// Plot the number of tracks
        /// </summary>
        public static IPlotSpec<double> jetPtRawPlot =
            MakePlotterSpec<double>(100, 100.0, 130.0, pt => pt, "jetpt{0}", "Jet pT distribution {0}; p_T [GeV]");
        public static IPlotSpec<treeJets> JetPtPlot;

        public static IPlotSpec<double> bdtOutput =
            MakePlotterSpec<double>(1000, -1.0, 1.0, m => m, "bdt_{0}", "BDT Output for {0}; BDT Weight");

        /// <summary>
        /// Init the plot specs that have to be built on the fly. Seriously, I need a better way.
        /// </summary>
        static PlottingUtils()
        {
            JetPtPlot = jetPtRawPlot.FromType<double, treeJets>(tj => tj.jetPt);
        }
        #endregion

        /// <summary>
        /// Calculate a ROC curve
        /// </summary>
        /// <param name="q"></param>
        /// <param name="g"></param>
        /// <returns></returns>
        public static NTGraph CalculateROC(NTH1 q, NTH1 g)
        {
            var nbins = q.NbinsX;
            var gluonValues = IncrementalByBin(g, nbins);
            var quarkValues = IncrementalByBin(q, nbins);

            var roc = new ROOTNET.NTGraph(nbins, gluonValues, quarkValues);
            roc.Xaxis.Title = g.Title;
            roc.Yaxis.Title = q.Title;
            roc.Title = "ROC curve";
            roc.Name = "roc";

            return roc;
        }

        /// <summary>
        /// Inremental version of the axies
        /// </summary>
        /// <param name="g"></param>
        /// <param name="nbins"></param>
        /// <returns></returns>
        private static double[] IncrementalByBin(NTH1 g, int nbins)
        {
            var values = Enumerable.Range(1, nbins)
                .Select(nbin => g.GetBinContent(nbin))
                .ToArray();

            double sum = 0.0;
            for (int index = 0; index < nbins; index++)
            {
                sum += values[index];
                values[index] = sum;
            }

            // And rescale and reverse because we cut ">".
            for (int i = 0; i < nbins; i++)
            {
                values[i] /= sum;
                values[i] = 1.0 - values[i];
            }

            return values;
        }

        /// <summary>
        /// Calculate the area under the curve. ROOT doesn't do what we want here.
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static double CalculateAUC(NTGraph g)
        {
            var npoints = g.N;
            var sum = 0.0;

            for (int i = 0; i < npoints; i++)
            {
                double x = 0.0, y = 0.0;
                g.GetPoint(i, ref x, ref y);
                sum += y / npoints;
            }
            return sum;
        }


    }
}
