#Load plotting
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score

#other tools
import numpy as np
import root_numpy
import pandas
import math
from pdb import set_trace
from glob import glob
import gc
from sklearn.preprocessing import StandardScaler
datafiles = {
    'gmod' : glob('/eos/project/i/iml/IMLChallengeQG/gluons_modified/*.root'),
    'gstd' : glob('/eos/project/i/iml/IMLChallengeQG/gluons_standard/*.root'),
    'qmod' : glob('/eos/project/i/iml/IMLChallengeQG/quarks_modified/*.root'),
    'qstd' : glob('/eos/project/i/iml/IMLChallengeQG/quarks_standard/*.root'),
}

print 'Sample  --- # of files'
for i, n in datafiles.iteritems():
    print i, '---',len(n)

@np.vectorize
def rings(jeta, jphi, comps_pt, comps_eta, comps_phi):
    drs = np.sqrt((jeta-comps_eta)**2+(jphi-comps_phi)**2)
    nbins = 10
    binid = np.floor(drs*nbins/0.4) #10 bins in DR
    retvals = [0. for i in range(nbins)]
    for pt, ibin in zip(comps_pt, binid):
        ibin = int(ibin)
        if ibin >= nbins: ibin = nbins-1
        retvals[ibin] += pt
    return tuple(retvals)

@np.vectorize
def sigmas(jpt, jeta, jphi, comps_pt, comps_eta, comps_phi):
    if not len(comps_pt):
        return 0., 0., 0.
    m11 = (comps_pt**2*(comps_eta-jeta)**2).sum()
    m22 = (comps_pt**2*(comps_phi-jphi)**2).sum()
    m12 = -1*(comps_pt**2*(comps_phi-jphi)*(comps_eta-jeta)).sum()
    ###
    t = m11+m22
    d = m11*m22 - pow(m12,2)
    #eigenvalues
    pow_val = pow(t,2)/4 - d
    if pow_val < 0:
        return 0., 0., 0.
    l1 = t/2 + math.sqrt(pow_val)    
    l2 = t/2 - math.sqrt(pow_val)
    sumpt = (comps_pt**2).sum()
    #sigmas
    sigma1 = math.sqrt(l1/sumpt)
    if l2 < 0:
        l2 = 0.
    sigma2 = math.sqrt(l2/sumpt)
    sigma = math.sqrt(pow(sigma1, 2)+pow(sigma2, 2))
    return sigma1, sigma2, sigma

@np.vectorize
def ptD(comps_pt):
    if not len(comps_pt):
        return 0.
    return math.sqrt((comps_pt**2).sum())/comps_pt.sum()

@np.vectorize
def jetEem(towerEem):
    return towerEem.sum()

@np.vectorize
def jetEhad(towerEem):
    return towerEem.sum()

scaler = None
def load(infiles, features, **kwargs):
    arr = pandas.DataFrame(
        root_numpy.root2array(
            infiles, 'treeJets',
            **kwargs
            )
        )
    s1, s2, s3 = sigmas(
        arr['jetPt'], arr['jetEta'], arr['jetPhi'],
        arr['trackPt'], arr['trackEta'], arr['trackPhi']
    )
    arr['trk_sigma1'] = s1
    arr['trk_sigma2'] = s2
    arr['trk_sigma' ] = s3
    arr['trk_ptD'] = ptD(arr['trackPt'])
    calo_s1, calo_s2, calo_s3 = sigmas(
        arr['jetPt'], arr['jetEta'], arr['jetPhi'], 
        arr['towerE'], arr['towerEta'], arr['towerPhi']
    )
    calo_ptDval = ptD(arr['towerE'])
    arr['calo_sigma1'] = calo_s1
    arr['calo_sigma2'] = calo_s2
    arr['calo_sigma' ] = calo_s3
    arr['calo_ptD'] = ptD(arr['towerE'])
    
    arr['jetEem']  = jetEem(arr['towerEem'])
    arr['jetEhad'] = jetEhad(arr['towerEhad'])

    trk_rings = rings(
        arr['jetEta'], arr['jetPhi'], 
        arr['trackPt'], arr['trackEta'], arr['trackPhi']
        )
    for idx, ring in enumerate(trk_rings):
        name = 'trk_ring%d' % idx
        arr[name] = ring

    hcal_rings = rings(
        arr['jetEta'], arr['jetPhi'], 
        arr['towerEhad'], arr['towerEta'], arr['towerPhi']
        )
    for idx, ring in enumerate(hcal_rings):
        name = 'hcal_ring%d' % idx
        arr[name] = ring

    hem_rings = rings(
        arr['jetEta'], arr['jetPhi'], 
        arr['towerEem'], arr['towerEta'], arr['towerPhi']
        )
    for idx, ring in enumerate(hem_rings):
        name = 'hem_ring%d' % idx
        arr[name] = ring

    arr = arr[features]
    if scaler:
        print 'scaling'
        cols = arr.columns
        arr = pandas.DataFrame(scaler.transform(arr))
        arr.columns = cols
    return arr

#
# Define features used for classification
#
feats = [
    'trk_sigma1', 'trk_sigma2', 'trk_sigma', 'trk_ptD',
    'calo_sigma1', 'calo_sigma2', 'calo_sigma', 'calo_ptD',
    'jetPt', 'jetEta', 'jetMass', 'ntracks', 'ntowers', 
    'jetEem', 'jetEhad'
]
for idx in range(10):
    feats.append('trk_ring%d' % idx)
    feats.append('hcal_ring%d' % idx)
    feats.append('hem_ring%d' % idx)


print 're-creating the scaler'
nfiles_for_bdt = 300
modset = load(datafiles['gmod'][:nfiles_for_bdt] + datafiles['qmod'][:nfiles_for_bdt], feats)
scaler = StandardScaler()
columns = modset.columns
modset = pandas.DataFrame(scaler.fit_transform(modset))
modset.columns = columns
print 'done'
del modset
#
# Make Keras model
#
import keras
from keras.utils import np_utils
def load_dataset(qfiles, gfiles):
    print 'loading files'
    quarks = load(qfiles, feats)    
    gluons = load(gfiles, feats)

    isQ = np.concatenate((np.ones(quarks.shape[0]), np.zeros(gluons.shape[0])))
    Y = np_utils.to_categorical(isQ.astype(int), 2) #isG, isQ
    X = pandas.concat([quarks, gluons])

    print 'computing weights'
    weights = None
    return X, Y, weights


model = keras.models.load_model('mverzett_v0_KERAS_model.h5')

qchunks =  np.array_split(np.array(datafiles['qmod']),100)
gchunks =  np.array_split(np.array(datafiles['gmod']),100)
test_y = np.array([])
test_p = np.array([])
for qch, gch in zip(qchunks, gchunks):
    print 'processing chunk'
    gc.collect()
    c_x, c_y, _ = load_dataset(qch, gch)
    c_p = model.predict(c_x.as_matrix())[:, 1]
    test_y = np.concatenate((test_y, c_y[:, 1].copy()))
    test_p = np.concatenate((test_p, c_p.copy()))
print 'ROC AUC on FULL TEST:', roc_auc_score(test_y,  test_p)
