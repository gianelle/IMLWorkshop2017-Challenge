import glob
import logging
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from numpy.lib.recfunctions import stack_arrays
import pandas as pd
from root_numpy import root2array
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import cPickle as pickle

N_TRACKS = 15
N_TOWERS = 40

def configure_logging():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logging.basicConfig(format="%(levelname)-8s\033[1m%(name)-21s\033[0m: %(message)s")
    logging.addLevelName(logging.WARNING,
        "\033[1;31m{:8}\033[1;0m".format(logging.getLevelName(logging.WARNING)))
    logging.addLevelName(logging.ERROR,
        "\033[1;35m{:8}\033[1;0m".format(logging.getLevelName(logging.ERROR)))
    logging.addLevelName(logging.INFO,
        "\033[1;32m{:8}\033[1;0m".format(logging.getLevelName(logging.INFO)))
    logging.addLevelName(logging.DEBUG,
        "\033[1;34m{:8}\033[1;0m".format(logging.getLevelName(logging.DEBUG)))

def root2pandas(file_paths, tree_name, **kwargs):
    '''
    Args:
    -----
        files_path: a string like './data/*.root', for example
        tree_name: a string like 'Collection_Tree' corresponding to the name of 
                   the folder inside the root file that we want to open
        kwargs: arguments taken by root2rec, such as branches to consider, etc
    Returns:
    --------
        output_panda: a panda dataframe like allbkg_df in which all the info 
                      from the root file will be stored
    Note:
    -----
        if you are working with .root files that contain different branches,
        you might have to mask your data
        in that case, return pd.DataFrame(ss.data)
    '''
    if isinstance(file_paths, basestring):
        files = glob.glob(file_paths)
    else:
        files = [matched_f for f in file_paths for matched_f in glob.glob(f)]

    ss = stack_arrays([root2array(fpath, tree_name, **kwargs).view(np.recarray) for fpath in files])
    try:
        return pd.DataFrame(ss)
    except Exception:
        return pd.DataFrame(ss.data)
    
def flatten(column):
    '''
    Args:
    -----
        column: a column of a pandas df whose entries are lists 
                (or regular entries -- in which case nothing is done)
                e.g.: my_df['some_variable'] 
    Returns:
    --------    
        flattened out version of the column. 
        For example, it will turn:
        [1791, 2719, 1891]
        [1717, 1, 0, 171, 9181, 537, 12]
        [82, 11]
        ...
        into:
        1791, 2719, 1891, 1717, 1, 0, 171, 9181, 537, 12, 82, 11, ...
    '''
    try:
        return np.array([v for e in column for v in e])
    except (TypeError, ValueError):
        return column

def sort_tracks(df, data, sort_by, n_tracks):
    ''' 
    Definition:
        Sort tracks by sort_by and put them into an ndarray called data.
        Pad missing tracks with -999 --> net will have to have Masking layer
    
    Args:
    -----
        df: a dataframe
        data: an array of shape (nb_samples, nb_tracks, nb_features)
        sort_by: a string representing the column to sort the tracks by
        n_tracks: number of tracks to cut off at. if >, truncate, else, -999 pad
    
    Returns:
    --------
        modifies @a data in place. Pads with -999
    
    '''
    import tqdm
    for i, jet in tqdm.tqdm(df.iterrows()):

        # tracks = [[pt's], [eta's], ...] of tracks for each jet 
        tracks = np.array(
                [v.tolist() for v in jet.get_values()],
                dtype='float32'
            )[:, (np.argsort(jet[sort_by]))[::-1]]
    
        # total number of tracks per jet      
        ntrk = tracks.shape[1] 

        # take all tracks unless there are more than n_tracks 
#         print data.shape
#         print tracks.shape
        data[i, :(min(ntrk, n_tracks)), :] = tracks.T[:(min(ntrk, n_tracks)), :] 

        # default value for missing tracks 
        data[i, (min(ntrk, n_tracks)):, :  ] = -999 


def scale(data, var_names, sort_by, file_name, savevars):
    ''' 
    Args:
    -----
        data: a numpy array of shape (nb_samples, nb_tracks, n_variables)
        var_names: list of keys to be used for the model
        sort_by: string with the name of the column used for sorting tracks
                 needed to return json file in format requested by keras2json.py
        file_name: str, tag that identifies the specific way in which the data was prepared, 
                   i.e. '30trk_hits'
        savevars: bool -- True for training, False for testing
                  it decides whether we want to fit on data to find mean and std 
                  or if we want to use those stored in the json file 
    Returns:
    --------
        modifies data in place
    '''    
    import json
    scale = {}
    if savevars:  
        for v, vname in enumerate(var_names):
            print 'Scaling feature %s of %s (%s).' % (v, len(var_names), vname)
            f = data[:, :, v]
            slc = f[f != -999]
            # -- first find the mean and std of the training data
            m, s = slc.mean(), slc.std()
            # -- then scale the training distributions using the found mean and std
            slc -= m
            slc /= s
            data[:, :, v][f != -999] = slc.astype('float32')
            scale[v] = {'name' : vname, 'mean' : m, 'sd' : s}

        # -- write variable json for lwtnn keras2json converter
        variable_dict = {
            'inputs': [{
                'name': scale[v]['name'],
                'scale': float(1.0 / scale[v]['sd']),
                'offset': float(-scale[v]['mean']),
                'default': None
                } 
                for v in xrange(len(var_names))],
            'class_labels': ['isGluon'],
            'miscellaneous': {
                'sort_by': sort_by
            }
        }
        with open('var' + file_name + '.json', 'wb') as varfile:
            json.dump(variable_dict, varfile)

    # -- when operating on the test sample, use mean and std from training sample
    # -- this info is stored in the json file
    else:
        with open('var' + file_name + '.json', 'rb') as varfile:
            varinfo = json.load(varfile)

        for v, vname in enumerate(var_names):
            print 'Scaling feature %s of %s (%s).' % (v, len(var_names), vname)
            f = data[:, :, v]
            slc = f[f != -999]
            ix = [i for i in xrange(len(varinfo['inputs'])) 
                  if varinfo['inputs'][i]['name'] == vname]
            offset = varinfo['inputs'][ix[0]]['offset']
            scale = varinfo['inputs'][ix[0]]['scale']
            slc += offset
            slc *= scale
            data[:, :, v][f != -999] = slc.astype('float32')

            
def _process_data(df, n_subobjects, sort_by, file_name, savevars):
    X = np.zeros(
        (df.shape[0], n_subobjects, df.shape[-1]),
        dtype='float32'
    )
    # -- call functions above
    sort_tracks(df, X, sort_by, n_subobjects)
    scale(X, df.keys(), sort_by, file_name, savevars)
    # -- default values                                                                                                                                                                                          
    X[np.isnan(X)] = -999
    return X


def _run(dataset):
    '''
    dataset = 'standard' or 'modified'
    '''
    configure_logging()
    logger = logging.getLogger("Data Processing")
    # -- load dataframes
    logger.info('Loading quark files')
    quarks_df = root2pandas('../data/IMLChallengeQG/quarks_{dataset}/REDUCED_quarks_*.root'.format(dataset=dataset), 'treeJets')
    logger.info('Loading gluon files')
    gluons_df = root2pandas('../data/IMLChallengeQG/gluons_{dataset}/REDUCED_gluons_*.root'.format(dataset=dataset), 'treeJets')

    logger.info('Concatenating')
    y = np.concatenate((np.zeros(quarks_df.shape[0]), np.ones(gluons_df.shape[0])))
    df = pd.concat([quarks_df, gluons_df])
    del quarks_df, gluons_df

    logger.info('Building flat, tracks and towers dataframes')
    X_tracks_df = df[[k for k in df.keys() if k.startswith('track')]]
    X_towers_df = df[[k for k in df.keys() if k.startswith('tower')]]
    X_flat = df[df.columns[df.dtypes != 'object']]

    logger.info('Building pT ratios')
    denom = X_tracks_df['trackPt'].map(sum).values
    denom[denom < 1e-2] = 1e-2
    X_flat['jetPtRatio'] = X_flat['jetPt'] / denom
    denom = X_flat['jetPt'].values
    denom[denom < 1e-2] = 1e-2
    X_tracks_df['trackPtRatio'] = X_tracks_df['trackPt'] / denom

    X_flat = X_flat.values

    logger.info('Scaling flat features')
    if dataset == 'standard':
        sc = StandardScaler()
        X_flat = sc.fit_transform(X_flat)
        with open('standard_scaler.pickle', 'wb') as pkl:
            pickle.dump(sc, pkl, protocol=pickle.HIGHEST_PROTOCOL)
    elif dataset == 'modified':
        sc = pickle.load(open('standard_scaler.pickle', 'rb'))
        X_flat = sc.transform(X_flat)

    logger.info('Scaling towers and tracks')
    X_towers = _process_data(
        X_towers_df, N_TOWERS, 'towerE', '40towers', dataset == 'standard')
    logger.info('Scaling tracks train data')
    X_tracks = _process_data(
        X_tracks_df, N_TRACKS, 'trackPt', '15tracks', dataset == 'standard')
    
    del X_tracks_df, X_towers_df

    out_dict = {
        'X_tracks' : X_tracks,
        'X_towers' : X_towers,
        'X_flat' : X_flat,
        'y' : y
    }

    import deepdish.io as io
    import os
    logger.info('Saving HDF5')
    io.save(os.path.join('..', 'data', 'IMLChallengeQG', dataset + '.h5'), out_dict)


if __name__ == '__main__':
    import argparse
    import sys
    # -- read in arguments
    parser = argparse.ArgumentParser()
    # Look at the yaml file in this directory for an example
    parser.add_argument('dataset', type=str, help="standard or modified") # my design choice
   
    args = parser.parse_args()

    sys.exit(_run(args.dataset))



