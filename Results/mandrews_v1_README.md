# Results 

Pipeline for doing Quark/Gluon Classification

The model was trained using a python script but
its contents are reproduced in the ipython notebook.
In order to reproduce the results using the noteboook,
you will have to use the included CreateJetShapes files
to reprocess the data with the engineered features:

mv MAndrews_v1_CreateJetShapes.C ../Examples/CreateJetShapes.C

For convenience, the keras model file for the network
with the trained weights is included and which the notebook
loads by default.

Included files:
MAndrews_v1_auc.txt
MAndrews_v1_CreateJetShapes.C
MAndrews_v1_main.ipynb
MAndrews_v1_model.h5
