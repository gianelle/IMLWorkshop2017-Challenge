
import ROOT

# Set up TMVA factory
output_file = ROOT.TFile.Open('TMVAClassification.root', 'RECREATE')
factory = ROOT.TMVA.Factory('TMVAClassification', output_file,
        '!V:!Silent:Color:!DrawProgressBar:Transformations=I,G:'+\
        'AnalysisType=Classification')

# ## Load data
# NOTE: Check out the `preprocess_data.py` script shipped with this notebook!
#data = ROOT.TFile("preprocessed_data.root")
data = ROOT.TFile("preprocessed_data_tmp.root")
if data == None:
    raise Exception('Have you run the preprocessing? Can not open file: {}'.format(filename))

quarks = data.Get('quarks')
gluons = data.Get('gluons')

# Set up dataloader and book all branches in the trees as input variables
dataloader = ROOT.TMVA.DataLoader('TMVAClassification')
for branch in quarks.GetListOfBranches():
    # NOTE: Here we are restricting the number of variables with rules on the variable name
    if not '_' in branch.GetName():
        dataloader.AddVariable(branch.GetName())

dataloader.AddTree(quarks, 'quarks', 1.0)
dataloader.AddTree(gluons, 'gluons', 1.0)
dataloader.PrepareTrainingAndTestTree(ROOT.TCut(''),
        # NOTE: Use this config if you run on the full dataset
        'TrainTestSplit_quarks=0.8:TrainTestSplit_gluons=0.8:'+\
        #'nTrain_quarks=9000:nTest_quarks=9000:'+\
        #'nTrain_gluons=9000:nTest_gluons=9000:'+\
        'SplitMode=Random:NormMode=NumEvents:!V')


# ## Book MVA methods

# In[3]:

# Boosted Decision Tree
factory.BookMethod(dataloader, ROOT.TMVA.Types.kBDT, 'BDTG',
		'H:!V:VarTransform=None:NTrees=300:MaxDepth=2:nCuts=30:BoostType=Grad:Shrinkage=0.10')


# ## Train, test and evaluate methods

# In[5]:

factory.TrainAllMethods()


# In[6]:

factory.TestAllMethods()


# In[7]:

factory.EvaluateAllMethods()


# ## Get results
# 
# In the following, we are plotting the receiver operating characteristic, which integral - the so called area under curve or ROC integral - is the figure of merit for this challenge. The explicit value is read out below and can be extracted from the TMVA evaluation output above as well.
# 
# As well, this example comes with a script `RUN_TMVA_GUI`, which can be executed from any terminal with connection to a graphical interface. It will run ROOT and open the full TMVA evaluation with all information taken from the file `TMVAClassification.root`, that is created by running this notebook.

# In[8]:

# Plot ROC

# Enable JavaScript magic
#get_ipython().magic(u'jsroot on')

canvas = factory.GetROCCurve(dataloader)
canvas.Draw()
canvas.SaveAs("ROC.png")
canvas.SaveAs("ROC.pdf")


# In[9]:

# Print area-under-curve (ROC integral) for Fisher method
print('AUC BDTG: {0}'.format(factory.GetROCIntegral(dataloader, 'BDTG')))


# # Preparing the results submission
# 
# Please use the TMVAMeasureAUC.ipynb notebook to evaluate the AUC in the "modified" data set (for the results submission).

# In[ ]:



